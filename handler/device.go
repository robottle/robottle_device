package handler

import (
	"context"
	"encoding/base64"
	"fmt"
	"strings"
	"time"

	"github.com/jinzhu/gorm"

	commonRandom "gitlab.com/robottle/robottle_common/random"

	"gitlab.com/robottle/robottle_device/model"
	"gitlab.com/robottle/robottle_device/option"
	proto "gitlab.com/robottle/robottle_device/proto/device"
	"gitlab.com/robottle/robottle_device/repository"
	"gitlab.com/robottle/robottle_device/validate"
)

type DeviceHandler struct {
	deviceRepository *repository.DeviceRepository
}

func NewDeviceHandler(options *option.Options) proto.DeviceHandler {
	return &DeviceHandler{
		deviceRepository: options.DeviceRepository,
	}
}

func (h *DeviceHandler) CreateDevice(ctx context.Context, req *proto.DeviceRequest, res *proto.DeviceResponse) error {
	requestValidation := validate.DeviceRequestValidation{
		AccountID:   req.AccountId,
		Name:        req.Name,
		Description: req.Description,
	}
	if err := requestValidation.ValidateCreate(); err != nil {
		return err
	}
	slug, err := h.deviceRepository.GenerateSlug(req.Name)
	if err != nil {
		return err
	}
	deviceKey := base64.URLEncoding.EncodeToString([]byte(commonRandom.String(16)))
	deviceSecret := base64.URLEncoding.EncodeToString([]byte(commonRandom.String(32)))
	device := &model.Device{
		AccountID:    uint(req.AccountId),
		Name:         req.Name,
		Slug:         slug,
		Description:  req.Description,
		DeviceKey:    deviceKey,
		DeviceSecret: deviceSecret,
	}
	if err := h.deviceRepository.CreateDevice(device); err != nil {
		return err
	}
	res.Id = int64(device.ID)
	res.AccountId = int64(device.AccountID)
	res.Name = device.Name
	res.Slug = device.Slug
	res.Description = device.Description
	res.DeviceKey = device.DeviceKey
	res.DeviceSecret = device.DeviceSecret
	res.CreatedAt = device.CreatedAt.Format(time.RFC3339)
	res.UpdatedAt = device.UpdatedAt.Format(time.RFC3339)
	return nil
}

func (h *DeviceHandler) FindDeviceByDeviceKey(ctx context.Context, req *proto.FindDeviceRequest, res *proto.DeviceResponse) error {
	device, err := h.deviceRepository.FindDeviceByDeviceKey(req.DeviceKey)
	if gorm.IsRecordNotFoundError(err) {
		return fmt.Errorf("device with device key %s not found", req.DeviceKey)
	}
	res.Id = int64(device.ID)
	res.AccountId = int64(device.AccountID)
	res.Name = device.Name
	res.Slug = device.Slug
	res.Description = device.Description
	res.DeviceKey = device.DeviceKey
	res.DeviceSecret = device.DeviceSecret
	res.CreatedAt = device.CreatedAt.Format(time.RFC3339)
	res.UpdatedAt = device.UpdatedAt.Format(time.RFC3339)
	return nil
}

func (h *DeviceHandler) UpdateDevice(ctx context.Context, req *proto.DeviceRequest, res *proto.DeviceResponse) error {
	requestValidation := validate.DeviceRequestValidation{
		ID:          req.Id,
		AccountID:   req.AccountId,
		Name:        req.Name,
		Description: req.Description,
	}
	if err := requestValidation.ValidateCreate(); err != nil {
		return err
	}
	device, err := h.deviceRepository.FindDeviceByID(req.Id)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return fmt.Errorf("device with id %s not found", req.Id)
		}
		return err
	}
	updates := make(map[string]interface{})
	if len(strings.TrimSpace(req.Name)) > 0 {
		updates["name"] = req.Name
	}
	if len(strings.TrimSpace(req.Description)) > 0 {
		updates["description"] = req.Description
	}
	if len(updates) > 0 {
		if err := h.deviceRepository.UpdateDevice(device, updates); err != nil {
			return err
		}
	}
	res.Id = int64(device.ID)
	res.AccountId = int64(device.AccountID)
	res.Name = device.Name
	res.Slug = device.Slug
	res.Description = device.Description
	res.DeviceKey = device.DeviceKey
	res.DeviceSecret = device.DeviceSecret
	res.CreatedAt = device.CreatedAt.Format(time.RFC3339)
	res.UpdatedAt = device.UpdatedAt.Format(time.RFC3339)
	return nil
}

func (h *DeviceHandler) ListDevices(ctx context.Context, req *proto.ListDevicesRequest, res *proto.ListDevicesResponse) error {
	devices, err := h.deviceRepository.FindDevicesByAccountID(req.AccountId)
	if err != nil {
		return err
	}
	res.Devices = make([]*proto.DeviceResponse, len(devices))
	for i, device := range devices {
		res.Devices[i] = &proto.DeviceResponse{
			Id:           int64(device.ID),
			AccountId:    int64(device.AccountID),
			Name:         device.Name,
			Slug:         device.Slug,
			Description:  device.Description,
			DeviceKey:    device.DeviceKey,
			DeviceSecret: device.DeviceSecret,
			CreatedAt:    device.CreatedAt.Format(time.RFC3339),
			UpdatedAt:    device.UpdatedAt.Format(time.RFC3339),
		}
	}
	return nil
}

func (h *DeviceHandler) RegenerateKeys(ctx context.Context, req *proto.DeviceRequest, res *proto.DeviceResponse) error {
	device, err := h.deviceRepository.FindDeviceByID(req.Id)
	if err != nil {
		if gorm.IsRecordNotFoundError(err) {
			return fmt.Errorf("device with id %s not found", req.Id)
		}
		return err
	}
	deviceKey := commonRandom.String(16)
	deviceSecret := commonRandom.String(32)
	updates := map[string]interface{}{
		"device_key":    deviceKey,
		"device_secret": deviceSecret,
	}
	if err := h.deviceRepository.UpdateDevice(device, updates); err != nil {
		return err
	}
	res.Id = int64(device.ID)
	res.AccountId = int64(device.AccountID)
	res.Name = device.Name
	res.Slug = device.Slug
	res.Description = device.Description
	res.DeviceKey = device.DeviceKey
	res.DeviceSecret = device.DeviceSecret
	res.CreatedAt = device.CreatedAt.Format(time.RFC3339)
	res.UpdatedAt = device.UpdatedAt.Format(time.RFC3339)
	return nil
}
