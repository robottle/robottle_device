module gitlab.com/robottle/robottle_device

go 1.12

require (
	github.com/gin-contrib/cors v0.0.0-20190424000812-bd1331c62cae
	github.com/gin-gonic/gin v1.3.0
	github.com/go-ozzo/ozzo-validation v3.5.0+incompatible
	github.com/golang/protobuf v1.3.1
	github.com/gosimple/slug v1.5.0
	github.com/jinzhu/gorm v1.9.5
	github.com/micro/go-micro v1.1.0
	gitlab.com/robottle/robottle_account v0.0.0-20190503011625-7e0872a308a3
	gitlab.com/robottle/robottle_common v0.0.0-20190502075711-c703ea102562
)

replace gitlab.com/robottle/robottle_common => ../robottle_common

replace gitlab.com/robottle/robottle_account => ../robottle_account
