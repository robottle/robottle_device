package repository

import (
	"fmt"

	"github.com/gosimple/slug"
	"github.com/jinzhu/gorm"
	"gitlab.com/robottle/robottle_common/random"
	"gitlab.com/robottle/robottle_device/model"
)

// DeviceRepository manages devices.
type DeviceRepository struct {
	db *gorm.DB
}

// NewDeviceRepository returns a instance to `DeviceRepository`.
func NewDeviceRepository(db *gorm.DB) *DeviceRepository {
	return &DeviceRepository{
		db: db,
	}
}

func (r *DeviceRepository) CreateDevice(device *model.Device) (err error) {
	err = r.db.Create(&device).Error
	return
}

func (r *DeviceRepository) FindDeviceByID(id interface{}) (device *model.Device, err error) {
	device = &model.Device{}
	err = r.db.Find(&device, id).Error
	return
}

func (r *DeviceRepository) FindDeviceByDeviceKey(deviceKey string) (device *model.Device, err error) {
	device = &model.Device{}
	err = r.db.Where("device_key = ?", deviceKey).First(&device).Error
	return
}

func (r *DeviceRepository) UpdateDevice(device *model.Device, updates map[string]interface{}) (err error) {
	err = r.db.Model(&device).Update(updates).Error
	return
}

func (r *DeviceRepository) FindDevicesByAccountID(accountID interface{}) (devices []*model.Device, err error) {
	devices = make([]*model.Device, 0)
	err = r.db.Where("account_id = ?", accountID).Find(&devices).Error
	return
}

func (r *DeviceRepository) GenerateSlug(name string) (string, error) {
	result := make(chan string)
	errc := make(chan error)
	base := slug.Make(name)
	go func() {
		tentative := base
		for {
			exists := &model.Device{}
			if err := r.db.Where("slug = ?", tentative).First(&exists).Error; err != nil {
				if gorm.IsRecordNotFoundError(err) {
					result <- tentative
				} else {
					errc <- err
				}
				break
			}
			tentative = fmt.Sprintf("%s-%s", base, random.String(6))
		}
	}()
	select {
	case deviceSlug := <-result:
		return deviceSlug, nil
	case err := <-errc:
		return "", err
	}
}
