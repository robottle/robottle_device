package router

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"

	"gitlab.com/robottle/robottle_account/helper"

	"gitlab.com/robottle/robottle_device/option"
	proto "gitlab.com/robottle/robottle_device/proto/device"
	"gitlab.com/robottle/robottle_device/validate"
)

type DeviceHandler struct {
	deviceService proto.DeviceService
}

func NewDeviceHandler(options *option.Options) *DeviceHandler {
	return &DeviceHandler{
		deviceService: options.DeviceService,
	}
}

func (h *DeviceHandler) Create(ctx *gin.Context) {
	currentUser := helper.GetUser(ctx)

	req := &proto.DeviceRequest{}
	if err := ctx.ShouldBindJSON(req); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  err,
		})
		return
	}
	requestValidation := &validate.DeviceRequestValidation{
		AccountID:   currentUser.AccountId,
		Name:        req.Name,
		Description: req.Description,
	}
	if err := requestValidation.ValidateCreate(); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  err,
		})
		return
	}
	req.AccountId = currentUser.AccountId
	res, err := h.deviceService.CreateDevice(ctx, req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnprocessableEntity, gin.H{
			"status": http.StatusUnprocessableEntity,
			"error":  err,
		})
		return
	}
	ctx.JSON(http.StatusCreated, res)
}

func (h *DeviceHandler) Update(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 32)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  "invalid user id",
		})
		return
	}
	currentUser := helper.GetUser(ctx)
	req := &proto.DeviceRequest{}
	if err := ctx.ShouldBindJSON(req); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  err,
		})
		return
	}
	requestValidation := &validate.DeviceRequestValidation{
		ID:          id,
		AccountID:   currentUser.AccountId,
		Name:        req.Name,
		Description: req.Description,
	}
	if err := requestValidation.ValidateUpdate(); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  err,
		})
		return
	}
	req.Id = id
	req.AccountId = currentUser.AccountId
	res, err := h.deviceService.UpdateDevice(ctx, req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnprocessableEntity, gin.H{
			"status": http.StatusUnprocessableEntity,
			"error":  err,
		})
		return
	}
	ctx.JSON(http.StatusOK, res)
}

func (h *DeviceHandler) Index(ctx *gin.Context) {
	currentUser := helper.GetUser(ctx)
	req := &proto.ListDevicesRequest{
		AccountId: currentUser.AccountId,
	}
	res, err := h.deviceService.ListDevices(ctx, req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  err,
		})
		return
	}
	ctx.JSON(http.StatusOK, res)
}

func (h *DeviceHandler) RegenerateKeys(ctx *gin.Context) {
	id, err := strconv.ParseInt(ctx.Param("id"), 10, 32)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  "invalid user id",
		})
		return
	}
	currentUser := helper.GetUser(ctx)
	req := &proto.DeviceRequest{}
	//if err := ctx.ShouldBindJSON(req); err != nil {
	//	ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
	//		"status": http.StatusBadRequest,
	//		"error":  err,
	//	})
	//	return
	//}
	requestValidation := &validate.DeviceRequestValidation{
		ID:        id,
		AccountID: currentUser.AccountId,
	}
	if err := requestValidation.ValidateUpdate(); err != nil {
		ctx.AbortWithStatusJSON(http.StatusBadRequest, gin.H{
			"status": http.StatusBadRequest,
			"error":  err,
		})
		return
	}
	req.Id = id
	req.AccountId = currentUser.AccountId
	res, err := h.deviceService.RegenerateKeys(ctx, req)
	if err != nil {
		ctx.AbortWithStatusJSON(http.StatusUnprocessableEntity, gin.H{
			"status": http.StatusUnprocessableEntity,
			"error":  err,
		})
		return
	}
	ctx.JSON(http.StatusOK, res)
}
