package router

import (
	"gitlab.com/robottle/robottle_account/middleware"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/robottle/robottle_common/config"
	"gitlab.com/robottle/robottle_device/option"
)

func NewRouter(options *option.Options) *gin.Engine {
	router := gin.Default()
	if config.IsProduction() {
		gin.SetMode(gin.ReleaseMode)
	}
	router.RedirectTrailingSlash = true

	router.Use(cors.New(cors.Config{
		AllowAllOrigins: false,
		AllowOrigins:    options.AllowedHosts,
		AllowHeaders:    options.AllowedHeaders,
		AllowMethods:    options.AllowedMethods,
		ExposeHeaders: []string{
			"Content-Type",
			"Content-Length",
			"Access-Control-Allow-Headers",
			"Access-Control-Allow-Methods",
		},
		AllowOriginFunc: func(origin string) bool {
			return true
		},
	}))

	requireToken := middleware.RequireToken(options.AuthService)

	router.Use(requireToken)

	deviceHandler := NewDeviceHandler(options)

	device := router.Group("/device")

	devices := device.Group("/devices")
	{
		devices.GET("/", deviceHandler.Index)
		devices.POST("/", deviceHandler.Create)
		devices.PUT("/:id", deviceHandler.Update)
		devices.PUT("/:id/regenerate_keys", deviceHandler.RegenerateKeys)
	}

	return router
}
