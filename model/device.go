package model

import "github.com/jinzhu/gorm"

type Device struct {
	gorm.Model
	AccountID    uint   `gorm:"not null; index"`
	Name         string `gorm:"not null"`
	Slug         string `gorm:"not null; unique_index"`
	Description  string `sql:"type:text"`
	DeviceKey    string `gorm:"not null; unique_index"`
	DeviceSecret string `gorm:"not null;"`
}
