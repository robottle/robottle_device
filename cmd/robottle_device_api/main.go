package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/micro/cli"
	"github.com/micro/go-micro/client"
	"github.com/micro/go-web"

	accountProto "gitlab.com/robottle/robottle_account/proto/account"
	commonConfig "gitlab.com/robottle/robottle_common/config"
	commonWrapper "gitlab.com/robottle/robottle_common/wrapper"

	"gitlab.com/robottle/robottle_device/option"
	proto "gitlab.com/robottle/robottle_device/proto/device"
	"gitlab.com/robottle/robottle_device/router"
)

const (
	ServiceName = "io.coderoso.robottle.api.device"
	Version     = "0.0.1"
)

func main() {
	var (
		configurationFile string

		errc = make(chan error)
	)
	service := web.NewService(
		web.Name(ServiceName),
		web.Version(Version),
		web.Flags(
			cli.StringFlag{
				Name:        "config, c",
				Value:       "config.yml",
				Usage:       "Path to configuration file to use. Defaults to config.yml",
				EnvVar:      "CONFIG_FILE",
				Destination: &configurationFile,
			},
		),
		web.RegisterTTL(30*time.Second),
		web.RegisterInterval(10*time.Second),
	)
	_ = service.Init()

	if err := commonConfig.SetConfigurationFile(configurationFile); err != nil {
		log.Fatalf("error setting configuration file: %v", err)
	}

	config, err := commonConfig.GetConfig()
	if err != nil {
		log.Fatalf("error getting configuration: %v", err)
	}

	deviceServiceSettings, found := config.Service["device"]
	if !found {
		log.Fatal("no service configuration for device found")
	}

	accountServiceSettings, found := config.Service["account"]
	if !found {
		log.Fatalf("no service configuration for account found")
	}

	httpSettings := config.HTTP
	if httpSettings == nil {
		log.Fatal("HTTP settings not found")
	}

	_ = client.DefaultClient.Init(
		client.WrapCall(commonWrapper.NewAuthCallWrapper()),
	)

	authService := accountProto.NewAuthService(accountServiceSettings.URL(), client.DefaultClient)
	deviceService := proto.NewDeviceService(deviceServiceSettings.URL(), client.DefaultClient)

	options := option.NewOptions(
		option.WithAuthService(authService),
		option.WithDeviceService(deviceService),
		option.WithAllowedHosts(httpSettings.AllowedHosts),
		option.WithAllowedMethods(httpSettings.AllowedMethods),
		option.WithAllowedHeaders(httpSettings.AllowedHeaders),
	)

	service.Handle("/", router.NewRouter(options))

	go func() {
		if err := service.Run(); err != nil {
			errc <- err
		}
	}()

	go func() {
		c := make(chan os.Signal)
		signal.Notify(c, syscall.SIGTERM, syscall.SIGINT, syscall.SIGKILL)
		errc <- fmt.Errorf("%v", <-c)
	}()

	if err := <-errc; err != nil {
		log.Fatal(err)
	}
}
