package option

import (
	accountProto "gitlab.com/robottle/robottle_account/proto/account"

	proto "gitlab.com/robottle/robottle_device/proto/device"
	"gitlab.com/robottle/robottle_device/repository"
)

type Options struct {
	DeviceRepository *repository.DeviceRepository
	DeviceService    proto.DeviceService
	AuthService      accountProto.AuthService
	AllowedHosts     []string
	AllowedMethods   []string
	AllowedHeaders   []string
}

type Option func(*Options)

func WithDeviceRepository(deviceRepository *repository.DeviceRepository) Option {
	return func(options *Options) {
		options.DeviceRepository = deviceRepository
	}
}

func WithDeviceService(deviceService proto.DeviceService) Option {
	return func(options *Options) {
		options.DeviceService = deviceService
	}
}

func WithAuthService(authService accountProto.AuthService) Option {
	return func(options *Options) {
		options.AuthService = authService
	}
}

// WithAllowedHosts sets the allowed hosts.
func WithAllowedHosts(allowedHosts []string) Option {
	return func(options *Options) {
		options.AllowedHosts = allowedHosts
	}
}

// WithAllowedMethods sets the allowed methods.
func WithAllowedMethods(allowedMethods []string) Option {
	return func(options *Options) {
		options.AllowedMethods = allowedMethods
	}
}

// WithAllowedHeaders sets the allowed headers.
func WithAllowedHeaders(allowedHeaders []string) Option {
	return func(options *Options) {
		options.AllowedHeaders = allowedHeaders
	}
}

func NewOptions(opts ...Option) *Options {
	options := &Options{}

	for _, opt := range opts {
		opt(options)
	}

	return options
}
