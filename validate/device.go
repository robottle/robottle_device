package validate

import (
	"github.com/go-ozzo/ozzo-validation"
)

type DeviceRequestValidation struct {
	ID           int64  `json:"id"`
	AccountID    int64  `json:"account_id"`
	Name         string `json:"name"`
	Description  string `json:"description"`
	DeviceKey    string `json:"device_key"`
	DeviceSecret string `json:"device_secret"`
}

func (v *DeviceRequestValidation) ValidateCreate() (err error) {
	err = validation.ValidateStruct(
		v,
		validation.Field(&v.Name, validation.Required),
		validation.Field(&v.AccountID, validation.Required),
	)
	return
}

func (v *DeviceRequestValidation) ValidateUpdate() (err error) {
	err = validation.ValidateStruct(
		v,
		validation.Field(&v.ID, validation.Required),
		validation.Field(&v.AccountID, validation.Required),
	)
	return
}
