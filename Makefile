LDFLAGS := -ldflags="-s -w"
PKG_LIST := `go list ./... | awk '!/cmd/ && !/model/ && !/proto/ && !/option/ && !/vendor/'`
GO_FILES := `go list ./... | awk '!/proto/ && !/vendor/'`
ACCOUNT_BIN := ./bin/robottle_device
API_BIN := ./bin/robottle_device_api
CLIENT_BIN := ./bin/robottle_device_client

.PHONY: all dep build clean test lint proto

all: build

lint:
	golint -set_exit_status ${GO_FILES}

build: dep proto
	go build -i ${LDFLAGS} -o ${ACCOUNT_BIN} cmd/robottle_device/main.go

api: dep proto
	go build -i ${LDFLAGS} -o ${API_BIN} cmd/robottle_device_api/main.go

client: dep proto
	go build -i ${LDFLAGS} -o ${CLIENT_BIN} cmd/robottle_device_client/*.go

proto:
	protoc --proto_path=. \
	       --micro_out=. \
	       --go_out=. \
	       proto/**/*.proto

dep:
	go mod download

vendor:
	go mod vendor

msan:
	APP_ENV=test go test -msan -short ${PKG_LIST}

race: dep
	APP_ENV=test go test -race -short ${PKG_LIST}

test:
	APP_ENV=test go test -v -coverprofile=coverage.out ${PKG_LIST} ${TEST_ARGS}
	go tool cover -html=coverage.out -o coverage.html

clean:
	rm -rf bin/robottle_device
	rm -rf bin/robottle_device_api
	rm -rf bin/robottle_device_client
